function [bearing] = calculate_bearing(map_cor,states)
%calculate_bearing Calculates the bearing for given coordinate and state
%   Detailed explanation goes here

bearing = atan2(map_cor(2) - states(2), map_cor(1) - states(1)) - states(3);

end