 % Code referenced from class particle_localization.m

% Particle filter localization for Airplane
clear; clc;
tic
% Time
Tf = 20;
dt = 0.1;
T = 0:dt:Tf;

% Initial State
x0 = [0 0 0 -5 2]';

% Control inputs
u = ones(2, length(T));
u(1,:) = 5*u(1,:);
u(2,:)=0.3*u(2,:);

% Motion model disturbance
R = [0.00001 0 0 0 0; 
     0 0.00001 0 0 0; 
     0 0 0.0001 0 0;
     0 0 0 0.00001 0;
     0 0 0 0 0.00001];
[RE, Re] = eig(R);

% Measurement model noise/disturbance
Q = [0.01 0 0 0;
    0 0.01 0 0;
    0 0 0.01 0;
    0 0 0 0.01];
[QE, Qe] = eig(Q);

% Number of particles
D = 100;

% Prior - uniform over [-1 1] position, [-pi/6 pi/6] heading, [-5 5] for
% vx and [-2 2] for vy
% X = diag([0 0 0 0 0])*(2*rand(5,D) - 1);
X = ones(5, 100);
X(1,:) = X(1,:)*0;
X(2,:) = X(2,:)*0;
X(3,:) = X(3,:)*0;
X(4,:) = X(4,:)*-5;
X(5,:) = X(5,:)*2;

X0 = X;
Xp = X;
w = zeros(1,D);
% Particles noise
[pRE, pRe] = eig(20*R);

% Feature Map
map = [ -10 0;
        10 0;
        10 20;
        -10 20];

% Simulation Initializations
n = length(x0);
x = zeros(n,length(T));
x(:,1) = x0;
m = length(Q);
y = zeros(m,length(T));

figure(1);clf; hold on;
markers = plot(map(:,1),map(:,2),'ro', 'MarkerSize',10,'LineWidth',2, 'DisplayName', 'Markers');
true_state = plot(x(1,1),x(2,1), 'ro--', 'DisplayName', 'True State');
for dd=1:D
    particle = plot(X(1,dd),X(2,dd),'b.', 'DisplayName', 'Particles');
end
axis equal
axis([-115 15 -1 70]);
title('Particle Filter Localization for Airplane')
F(1) = getframe;

%% Setup Wind Estimate Figures
figure(2);clf; hold on;
axis equal
axis([0 21 -7 -2])
title('Wind State Estimation V_x')

figure(3); hold on;
axis equal
axis([0 21 0 5])
title('Wind State Estimation V_y')


%% Main loop
for t=2:length(T)
    %% Simulation
    % Generate a motion disturbance
    e = RE*sqrt(Re)*randn(n,1);
    % Update state
    x(:,t) = [x(1,t-1)+(u(1,t)*cos(x(3, t-1)) + x(4, t-1))*dt;
              x(2,t-1)+(u(1,t)*sin(x(3,t-1))+x(5,t-1))*dt;
              x(3,t-1)+u(2,t)*dt;
              x(4,t-1);
              x(5,t-1)] + e;
    
    % Take measurement
    % Generate measurement disturbance
    d = QE*sqrt(Qe)*randn(m,1);
    for i=1:4
        y(i,t) = atan2(map(i,2)-x(2,t), map(i,1) - x(1,t)) - x(3,t) + d(i);
    end
    
    %% Particle filter estimation
    for dd=1:D
        % Noise of particle estimation
        e = pRE*sqrt(pRe)*randn(n,1);
        Xp(:,dd) = [X(1,dd)+(u(1,t)*cos(X(3,dd)) + X(4, dd))*dt;
                    X(2,dd)+(u(1,t)*sin(X(3,dd))+X(5,dd))*dt;
                    X(3,dd)+u(2,t)*dt;
                    X(4,dd);
                    X(5,dd)] + e;
        
        % Noise of measurement
        d = QE*sqrt(Qe)*randn(m,1);
        
        hXp = zeros(4,1);
        for i=1:4
            hXp(i,1) = atan2(map(i,2) - Xp(2,dd), map(i,1) - Xp(1,dd)) - Xp(3,dd) + d(i);
        end
        w(dd) = mvnpdf(y(:,t), hXp, Q);
    end
    W = cumsum(w);
    for dd=1:D
        seed = max(W)*rand(1);
        X(:,dd) = Xp(:,find(W>seed,1));
    end


    %% Plot results
    % Plot x,y
    figure(1);
    plot(x(1,1:t),x(2,1:t), 'ro--')
    % Plot every 1 second
    if mod(T(t),1) == 0
        for dd=1:D
             plot(X(1,dd),X(2,dd),'b.', 'DisplayName', 'Particles');
        end
    end
    % Plot measurements
    first_meas = plot([x(1,t) x(1,t)+10*cos(y(1,t)+x(3,t))], [ x(2,t) x(2,t)+10*sin(y(1,t)+x(3,t))], 'c', 'DisplayName', 'Bearing 1');
    second_meas = plot([x(1,t) x(1,t)+10*cos(y(2,t)+x(3,t))], [ x(2,t) x(2,t)+10*sin(y(2,t)+x(3,t))], 'm', 'DisplayName', 'Bearing 2');
    third_meas = plot([x(1,t) x(1,t)+10*cos(y(3,t)+x(3,t))], [ x(2,t) x(2,t)+10*sin(y(3,t)+x(3,t))], 'y', 'DisplayName', 'Bearing 3');
    fourth_meas = plot([x(1,t) x(1,t)+5*cos(y(4,t)+x(3,t))], [ x(2,t) x(2,t)+5*sin(y(4,t)+x(3,t))], 'g', 'DisplayName', 'Bearing 4');
    hleglines = [markers(1), true_state(1), particle(1), first_meas(1) second_meas(1) third_meas(1) fourth_meas(1)];
    legend(hleglines, 'FontSize', 14)

    figure(2);
    for dd=1:D
        particle_wind_velocity_x = plot(T(t), X(4,dd),'b.', 'DisplayName', 'Particle');
    end
    
    figure(3);
    for dd=1:D
        particle_wind_velocity_y = plot(T(t), X(5,dd),'b.', 'DisplayName', 'Particle');
    end

    F(t) = getframe;
    
    
end
toc